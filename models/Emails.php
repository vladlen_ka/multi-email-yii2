<?php

namespace app\models;

use Yii;
use yii\validators\EmailValidator;
/**
 * This is the model class for table "emails".
 *
 * @property integer $id
 * @property string $emails
 */
class Emails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emails';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emails'], 'required'],
            [['emails'], 'string'],
            [['emails'], 'checkEmailList'],
        ];
    }

    public function checkEmailList($attribute, $params) {
        $validator = new EmailValidator;
        $emails = is_array($this->emails)? : explode(';', $this->emails);

        $validated_emails = array();

        foreach ($emails as $email) {
                $validator->validate($email)? : $this->addError($attribute, "$email is not a valid email.");

                if(Emails::find()->where(['emails' => $email])->one() !== NULL)
                    $this->addError($attribute, $email. ' already exists!');
        }

        $count_same_emails = array_count_values($emails);
        foreach ($count_same_emails as $email => $repeat_times) {
            ($repeat_times >= 2)? $this->addError($attribute, "You have inputed $email $repeat_times times!!") : '';
        }
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emails' => 'Emails',
        ];
    }
}
