<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Emails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emails-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php if ($errors){?>
    <div class="alert alert-danger">

	<?php foreach ($errors as $error_type => $error_array) { ?>
		<strong>Danger!</strong>
    	<?php foreach ($error_array as $key => $error_message) { ?>
    	<br><?= $key+1 ?>. <?= $error_message;?>
    	<?php }?>
	<?php }?>
	</div>
    <?php } ?>

    <?= $form->field($model, 'emails')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
