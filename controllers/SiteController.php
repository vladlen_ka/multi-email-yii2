<?php

namespace app\controllers;

use Yii;
use app\models\Emails;
use app\models\EmailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmailsController implements the CRUD actions for Emails model.
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Emails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Emails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Emails();
        
        $all_saved = false; 

        $errors = NULL;

        if ($model->load(Yii::$app->request->post())) {

            $email_list = explode(';',$model->emails);

            $all_saved = true; 

            if(!empty($email_list) && is_array($email_list)) {

                $valid = $model->validate();

                $errors = $model->errors;
                if ($valid) {

                    foreach ($email_list as  $email) {

                        $new_model = new Emails();
                        $new_model->emails = $email;
                        $new_model->save();
                            
                    }

                } else {
                    $all_saved = false;
                }
            }

            
        }

        if($all_saved) {
            return $this->redirect(['index']);
        } else {
            

            return $this->render('create', [
                'model' => $model,
                'errors' => $errors,
            ]);
        }
    }

    /**
     * Finds the Emails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Emails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Emails::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
